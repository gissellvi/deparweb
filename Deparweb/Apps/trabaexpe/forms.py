from django.db import models
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import *

# class UsuarioForm(UserCreationForm):
#     first_name = forms.CharField(max_length=30, required=True)
#     last_name = forms.CharField(max_length=30, required=True)
#     email = forms.CharField(max_length=30, required=True)

#     class Meta:
#         model = User
#         fields = ("email", "first_name", "last_name", "password1", "password2")

class TrabajadorForm(forms.ModelForm):
	class Meta:
		model = Trabajador
		fields = [
			"cedula",
			"nombre",
			"apellido",
			"cod_tele",
			"telefono",
			"sexo",
			"num_ric",
			"fecha_ingre_publi",
			"fecha_ingre_institu",
			"cod_rac",
			"cargos",
			"correo",
			"estat_traba",
		]

class ExpedientesForm(forms.ModelForm):
	class Meta:
		model = Expedientes
		fields = [
			"cod_expe",
			"titulo",
			"parti_nacimiento",
			"evalua_empleado",
			"evalua_obrero",
			"evalua_comp",
			"califi_final",
			"solici_fide_comiso",
			"recibo_pago",
			"regis_censo_pers",
			"sintesis_curri",
			"cart_cuncub",
			"constancias",
			"trabajador",
			"sueldo",
		]
			

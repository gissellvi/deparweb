from django.conf.urls import url
from .views import *
from . import *

urlpatterns = [
	url(r"^admin/$", PanelAdministrativo, name="Admin"),
	url(r'^agg/$', AggWorker, name="Aggm"),
]
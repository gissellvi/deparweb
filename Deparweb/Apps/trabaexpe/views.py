from django.shortcuts import render
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .forms import *
from .models import *
import time
from django.views.generic import CreateView, TemplateView, View 


# Create your views here.
def PanelAdministrativo(request):
    return render(request,'panelAdmin/index.html')

def AggWorker(request):
	
	if request.user.is_staff:
		if request.method == 'POST':
			form = TrabajadorForm(request.POST)
			if form.is_valid():
				form.save()
			else:
				return render(request, "panel/aggtrabajador.html", {"trabajador":form})
			return HttpResponseRedirect("/")
		else:
			form = TrabajadorForm()
		return render(request,'panel/aggtrabajador.html',{'trabajador':form})
	else:
		return render(request, "administrador.html", {})
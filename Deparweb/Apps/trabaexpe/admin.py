# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import *


class AdminTrabajador(admin.ModelAdmin):
	list_display = ["cedula", "nombre", "apellido", "cod_tele","telefono", "sexo", "num_ric", "fecha_ingre_publi", "fecha_ingre_institu", "cod_rac", "cargos",  "correo", "estat_traba"]

admin.site.register(Trabajador, AdminTrabajador)
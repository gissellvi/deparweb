from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from Apps.constancias.models import *

class Trabajador(models.Model):
	cargo = (
		("Analista de Vacaciones","Analista de Vacaciones"),
		("Analista de Jubilacion", "Analista de Jubilacion"),
		("Analista de Seguro Social", "Analista de Seguro Social"),
		("Empleado (a) ","Empleado (a)"),
		("Medico (a) ","Medico (a)"),
		("Obrero (a) ","Obrero (a)"),
	)
	codigo=(
		("0424","0424"),
		("0414","0414"),
		("0426","0426"),
		("0416","0416"),
		("0412","0412"),
		("0257","0257"),
	)
	sexo=(
		("Femenino","Femenino"),
		("Masculino","Masculino"),
	)
	estatus=(
		("Activo","Activo"),
		("Inactivo","Inactivo"),
	)
	cedula = models.IntegerField(unique=True, null=True, blank=True)
	nombre = models.CharField(max_length=30)
	apellido = models.CharField(max_length=30)
	telefono = models.IntegerField(null=True,blank=True)
	cod_tele = models.CharField(max_length=20,choices=codigo)
	sexo = models.CharField(max_length=50, choices=sexo)
	num_ric = models.IntegerField(null=True, blank=True)
	fecha_ingre_publi = models.CharField(max_length=10)
	fecha_ingre_institu = models.CharField(max_length=10)
	cod_rac = models.IntegerField(null=True, blank=True)
	cargos = models.CharField(max_length=50, choices=cargo)
	correo =  models.CharField(max_length=50)
	estat_traba = models.CharField(max_length=50, choices=estatus)
	

	def __str__(self):
		return '%s - %s %s %s' %(self.cedula,self.nombre,self.apellido,self.cargos)

class Expedientes(models.Model):
	cod_expe = models.IntegerField(unique=True, null=True, blank=True)
	titulo = models.BooleanField()
	parti_nacimiento = models.BooleanField()
	evalua_empleado = models.BooleanField()
	evalua_obrero = models.BooleanField()
	evalua_comp = models.BooleanField()
	califi_final = models.BooleanField()
	solici_fide_comiso = models.BooleanField()
	recibo_pago = models.BooleanField()
	regis_censo_pers = models.BooleanField()
	sintesis_curri = models.BooleanField()
	cart_cuncub = models.BooleanField()
	trabajador = models.OneToOneField(Trabajador,on_delete=models.CASCADE)
	desca_trimes = models.BooleanField()
	sueldo = models.IntegerField(null=True, blank=True)
	constancias = models.ForeignKey(Constancias,on_delete=models.CASCADE)
	

	def __str__(self):
		return '%s - %s %s %s' %(self.cedula,self.nombre,self.apellido,self.cargos)
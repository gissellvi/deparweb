from django.db import models
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import *


class FirmanteForm(forms.ModelForm):
	class Meta:
		model = Firmante
		fields = [
			"cod_firm",
			"nomb_firm",
			"apel_firm",
			"cargos1",
		]


class JubilacionForm(forms.ModelForm):
	class Meta:
		model = Jubilacion
		fields = [
			"cod_jubi",
			"soli_jubi",
			"solic_camb",
		]

class Descanso_TrimestralForm(forms.ModelForm):
	class Meta:
		model = Descanso_Trimestral
		fields = [
			"cod_des_tri",
			"fecha_des_tri",
			"carg_libre_des_tri",
			"asistencia_des_tri",
			"observacion",
			"durac_permi_des_tri",
		]

class VacacionesForm(forms.ModelForm):
	class Meta:
		model = Vacaciones
		fields = [
			"cod_vaca",
			"soli_vaca",
			"perio_vaca_disfru",
			"dias_habi",
			"fecha_culmi",
			"observacion",
		]

class Seguro_SocialForm(forms.ModelForm):
	class Meta:
		model = Seguro_Social
		fields = [
			"cod_seg",
			"inscr_seg",
			"esta_seg",
			"fecha_func",
			"cons_1402",
			"cons_1403",
			"trami_pens",
		]

class ConstanciasForm(forms.ModelForm):
	class Meta:
		model = Constancias
		fields = [
			"cod_cons",
			"cons_igres",
			"cons_sueldo",
			"cons_ley_poli_habit",
			"seguro_social",
			"vacaciones",
			"desc_trimes",
			"jubilacion",
			"firmante",
		]
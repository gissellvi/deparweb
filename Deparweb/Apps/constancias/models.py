from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Firmante(models.Model):
	carg = (
		("Director (a) Hospital Miguel Oraa", 
		 "Director (a) Hospital Miguel Oraa"),
		
		("Director (a) Recursos Humanos Hospital Miguel Oraa", 
		 "Director (a) Recursos Humanos Hospital Miguel Oraa"),
		
		("Director (a) Region", "Director (a) Region"),
		("Director (a) Recursos Humanos Region ", 
		 "Director (a) Recursos Humanos Region"),

		("Archivera (o)", "Archivera (o)"),
	)
	ced_firm = models.IntegerField(unique=True, null=True, blank=True)
	nomb_firm = models.CharField(max_length=30)
	apel_firm = models.CharField(max_length=30)
	cargos1 = models.CharField(max_length=50, choices=carg)
	

	def __str__(self):
		return '%s - %s %s %s' %(self.ced_firm,self.nomb_firm,self.apel_firm,self.cargos1)

class Descanso_Trimestral(models.Model):
	cod_des_tri = models.IntegerField(unique=True, null=True, blank=True)
	fecha_des_tri = models.CharField(max_length=10)
	carg_libre_des_tri = models.CharField(max_length=30)
	asistencia_des_tri = models.CharField(max_length=30)
	observacion = models.CharField(max_length=50)
	durac_permi_des_tri = models.IntegerField()

	def __str__(self):
		return '%s - %s %s %s ' %(self.cod_des_tri,self.fecha_des_tri,
		self.observacion,self.durac_permi_des_tri)

class Vacaciones(models.Model):
	cod_vaca = models.IntegerField(unique=True, null=True, blank=True)
	soli_vaca = models.BooleanField()
	perio_vaca_disfru = models.CharField(max_length=10)
	dias_habi = models.IntegerField(null=True, blank=True)
	fecha_culmi = models.CharField(max_length=10)
	observacion = models.CharField(max_length=50)

	def __str__(self):
		return '%s - %s %s %s %s %s' %(self.cod_vaca,self.soli_vaca,
		self.perio_vaca_disfru,self.dias_habi,self.fecha_culmi,
		self.observacion)

class Seguro_Social(models.Model):
	estatus_seg = (
		("Egreso","Egreso"),
		("Ingreso", "Ingreso"),
	)
	cod_seg = models.IntegerField(unique=True, null=True, blank=True)
	inscr_seg = models.BooleanField()
	esta_seg = models.CharField(max_length=20,choices=estatus_seg)
	fecha_func = models.CharField(max_length=10)
	cons_1402 = models.BooleanField()
	cons_1403 = models.BooleanField()
	trami_pens = models.BooleanField()

	def __str__(self):
		return '%s - %s %s %s' %(self.cod_seg,self.inscr_seg,
		self.fecha_func,self.trami_pens)

class Jubilacion(models.Model):
	cod_jubi = models.IntegerField(unique=True, null=True, blank=True)
	soli_jubi = models.BooleanField()
	solic_camb = models.BooleanField()

	def __str__(self):
		return '%s - %s %s ' %(self.cod_jubi,self.soli_jubi,
		self.solic_camb)


class Constancias(models.Model):
	cod_cons = models.IntegerField(unique=True, null=True, blank=True)
	cons_igres = models.BooleanField()
	cons_sueldo = models.BooleanField()
	cons_ley_poli_habit = models.BooleanField()
	seguro_social = models.OneToOneField(Seguro_Social,on_delete=models.CASCADE)
	vacaciones = models.OneToOneField(Vacaciones,on_delete=models.CASCADE)
	desc_trimes = models.OneToOneField(Descanso_Trimestral,on_delete=models.CASCADE)
	jubilacion = models.OneToOneField(Jubilacion,on_delete=models.CASCADE)
	firmante = models.ManyToManyField(Firmante)

	def __str__(self):
		return '%s - %s %s %s %s %s %s' %(self.cod_cons,self.cons_igres,
		self.cons_sueldo,self.cons_ley_poli_habit,self.seguro_social,
		self.vacaciones,self.desc_trimes)



